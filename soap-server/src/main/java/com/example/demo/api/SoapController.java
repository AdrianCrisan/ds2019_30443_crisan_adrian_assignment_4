package com.example.demo.api;

import com.example.demo.medicalassistance.*;
import com.example.demo.model.Activities;
import com.example.demo.model.Recommendation;
import com.example.demo.repo.ActivityRepository;
import com.example.demo.repo.RecommendationRepository;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class SoapController {

    private static final String NAMESPACE_URI = "http://medicalAssistance/activity";

    private final ActivityRepository activityRepository;

    private final RecommendationRepository recommendationRepository;

    public SoapController(ActivityRepository activityRepository, RecommendationRepository recommendationRepository) {
        this.activityRepository = activityRepository;
        this.recommendationRepository = recommendationRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivityRequest")
    @ResponsePayload
    public GetActivityResponse getActivities(
            @RequestPayload GetActivityRequest request) {
        GetActivityResponse getActivityResponse =
                new GetActivityResponse();

        List<Activities> activities = activityRepository.getAllByPatientId(request.getPatientId());
        activities.forEach(activity -> getActivityResponse.getActivities().add(mapActivity(activity)));

        return getActivityResponse;
    }
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "postPatientRecommendationRequest")
    @ResponsePayload
    public PostPatientRecommendationResponse postRecommendation(
            @RequestPayload PostPatientRecommendationRequest request) {
        PostPatientRecommendationResponse postPatientRecommendationResponse = new PostPatientRecommendationResponse();

        Recommendation recommendation = new Recommendation();
        recommendation.setId(request.getId());
        recommendation.setPatientId(request.getPatientId());
        recommendation.setRecommendation(request.getRecommendation());
        activityRepository.save(findActivity(recommendation));

        postPatientRecommendationResponse.setRecommendations(mapRecommendation(recommendation));

        return postPatientRecommendationResponse;
    }

    private Activities findActivity(Recommendation r) {

        Activities a = activityRepository.findById((long) r.getId()).get();
        a.setRecommend(r.getRecommendation());
        a.setNormal(true);

        return a;
    }

    private Activity mapActivity(Activities activity) {
        Activity soapActivity = new Activity();
        soapActivity.setDuration(activity.getDuration());
        soapActivity.setId((int) activity.getId());
        soapActivity.setType(activity.getActivity());
        soapActivity.setDuration(activity.getDuration());
        soapActivity.setDay(activity.getDay());
        soapActivity.setRecommend(activity.getRecommend());
        soapActivity.setNormal(activity.isNormal());

        return soapActivity;
    }

    private com.example.demo.medicalassistance.Recommendation mapRecommendation(Recommendation recommendation) {
        com.example.demo.medicalassistance.Recommendation recommendation1 = new com.example.demo.medicalassistance.Recommendation();
        recommendation1.setId(recommendation.getId());
        recommendation1.setPatientId(recommendation.getPatientId());
        recommendation1.setRecommendation(recommendation.getRecommendation());

        return recommendation1;
    }
}
