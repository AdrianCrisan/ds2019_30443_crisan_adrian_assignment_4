package com.example.demo.repo;

import com.example.demo.model.Activities;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActivityRepository extends JpaRepository<Activities, Long> {

    List<Activities> getAllByPatientId(int patientId);
}
